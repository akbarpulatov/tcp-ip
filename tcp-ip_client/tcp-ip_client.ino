
#include <Ethernet.h>
#include <SPI.h>

//+
#define USERBUTTON1 6
#define USERBUTTON2 3
#define USERBUTTON3 2

#define ALARMRED    9
#define ALARMYELLOW 8
#define ALARMGREEN  7

unsigned char ucData[3];
//+

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 192, 168, 1, 177 };
byte server[] = { 192, 168, 1, 6 }; 

EthernetClient client;

void setup()
{
//+
  pinMode(USERBUTTON1, INPUT);
  pinMode(USERBUTTON2, INPUT);
  pinMode(USERBUTTON3, INPUT);
  
  pinMode(ALARMRED, INPUT);
  pinMode(ALARMYELLOW, INPUT);
  pinMode(ALARMGREEN, INPUT);
//+

  
  Ethernet.begin(mac, ip);
  Serial.begin(9600);

  delay(1000);

  Serial.println("connecting...");

  if (client.connect(server, 80)) {
    Serial.println("connected");
    client.println("GET /search?q=arduino HTTP/1.0");
    client.println();
  } else {
    Serial.println("connection failed");
  }
}

void loop()
{
//+
  ucData[0] = '0'+ (unsigned char)(((!digitalRead(ALARMRED)) << 2) | 
                                   ((!digitalRead(ALARMYELLOW)) << 1) |
                                   (!digitalRead(ALARMGREEN)));

  ucData[1] ='0'+ (unsigned char)(((!digitalRead(USERBUTTON1)) << 2) | 
                                  ((!digitalRead(USERBUTTON2)) << 1) |
                                  (!digitalRead(USERBUTTON3)));
  ucData[2] = 0;  
//+
  
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }


    
  if (client.connect(server, 80)) {
    Serial.println("connected");
    client.print(ucData[0]-'0');
    client.print(ucData[1]-'0');
    client.println();
  } else {
    Serial.println("connection failed");
  }

  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
    for(;;)
      ;
  }
}
